package com.example.demo.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

//@RequestMapping("/comment")
@Controller
public class CommentController {
	@Autowired
	CommentService commentService;
	@Autowired
	ReportService reportService;

	// 返信画面
	@GetMapping("/comment/new/{id}")
	public ModelAndView newComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// 返信用の空のentityを準備
		Comment comment = new Comment();
		comment.setReportId(id);
		// 画面遷移先を指定
		mav.setViewName("comment/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", comment);
		return mav;
	}

	// 返信処理
	@PostMapping("/comment/add")
	public ModelAndView addComment(@ModelAttribute("formModel") Comment comment) {
		// 返信をテーブルに格納
		System.out.println(comment);
		commentService.saveComment(comment);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 編集画面
	@GetMapping("/comment/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// 取得したIDに応じた投稿を取得
		Comment comment = commentService.editComment(id);
		//編集する投稿を表示
		mav.addObject("formModel", comment);
		// 画面遷移先を指定
		mav.setViewName("comment/edit");
		return mav;
	}

	// 編集処理
	@PutMapping("/comment/update/{id}")
	public ModelAndView updateContent(@PathVariable Integer id, @ModelAttribute("formModel") Comment comment) throws ParseException {
		// UrlParameterのidを更新するentityにセット
		comment.setId(id);
		//現在時刻を取得（Date型）
		Date date = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentDate = simpleDateFormat.format(date);
		Date updatedDate = simpleDateFormat.parse(currentDate);
		comment.setUpdatedDate(updatedDate);
		// 編集した投稿を更新
		commentService.saveComment(comment);
		// コメントから投稿IDを取得
		int reportId =comment.getReportId();
		// 指定したIDで投稿を取得
		Report report = reportService.editReport(reportId);
		// 取得した投稿に更新日時をセット
		report.setUpdatedDate(updatedDate);
		// 投稿日時のみ更新された投稿を保存
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 削除処理
	@DeleteMapping("/comment/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		// 投稿をテーブルから削除
		commentService.deleteComment(id);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}
}
