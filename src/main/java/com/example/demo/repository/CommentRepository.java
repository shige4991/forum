package com.example.demo.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer>{
	// 降順に表示
	@Query(value = "SELECT * FROM comment ORDER BY updated_date DESC", nativeQuery=true)
	List<Comment> findAllComment();



//	@Modifying
//    @Query("UPDATE report r SET r.updated_date = :itemName  o.date < :orderedDate")
//    int SetItemBefore(@Param("orderedDate")Date date, @Param("itemName")String newItemName);

//	@Query(value = "UPDATE report SET updated_date = ?1 WHERE id = ?2" , nativeQuery=true)
//	void reportSave(Date updatedDate, int id);

	// ?1 = commentのupdated_date、?2 = commentのreport_id
//	@Query(value = "UPDATE report SET updated_date = ?1 WHERE id = ?2" , nativeQuery=true)
//	void saveComment(Date updatedDate, int reportId);
}
