package com.example.demo.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Report;

@Repository
public interface ReportRepository extends JpaRepository<Report, Integer> {
	// 日付で検索
	@Query(value = "SELECT * FROM report WHERE created_date BETWEEN ?1 AND ?2", nativeQuery=true)
	List<Report> findReportByDate(Date start, Date end);
}