package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;
import com.example.demo.repository.ReportRepository;

@Service
public class CommentService {
	@Autowired
	CommentRepository commentRepository;
	ReportRepository reportRepository;

	// レコード取得
	public List<Comment> findAllComment() {
		return commentRepository.findAll(Sort.by(Sort.Direction.DESC, "updatedDate"));
	}

	// レコード追加、更新
	public void saveComment(Comment comment) {
		commentRepository.save(comment);
	}

	// 編集するレコードの取得
	public Comment editComment(Integer id) {
		return commentRepository.findById(id).orElse(null);
	}

	// レコード削除
	public void deleteComment(Integer id) {
		commentRepository.deleteById(id);
	}
}
