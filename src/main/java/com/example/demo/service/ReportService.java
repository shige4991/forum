package com.example.demo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {
	@Autowired
	ReportRepository reportRepository;
		// レコード全件取得
	public List<Report> findAllReport() {
		return reportRepository.findAll(Sort.by(Sort.Direction.DESC, "updatedDate"));
	}

	// レコード追加
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	// レコード削除
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}

	// 編集するレコードの取得
	public Report editReport(Integer id) {
		return reportRepository.findById(id).orElse(null);
	}

	// 日付による検索
	public List<Report> findReportByDate(String startDate, String endDate) throws ParseException{
		Date defaultEndDate = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// 開始日時が入力されていなければ初期値をセット
		if(StringUtils.isBlank(startDate)) {
			startDate = "2022-01-01 00:00:00";
		}
		else {
			startDate = startDate + " " + "00:00:00";
		}
		// 終了日時が入力されていなければ初期値（現在時刻）をセット
		if(StringUtils.isBlank(endDate)) {
		   endDate = simpleDateFormat.format(defaultEndDate);
		}
		else {
			endDate = endDate + " " + "23:59:59";
		}

		Date start = simpleDateFormat.parse(startDate);
		Date end = simpleDateFormat.parse(endDate);

		return reportRepository.findReportByDate(start, end);
	}

}
